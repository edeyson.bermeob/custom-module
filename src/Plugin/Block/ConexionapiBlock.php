<?php

namespace Drupal\conexion_api\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'Conexión api' Block
 * @Block(
 *   id = "Conexionapi Block",
 *   admin_label = @translation("Conexionapi Block"),
 *   category = @translation("Conexionapi Block"),
 * )
 * 
 */

 class ConexionapiBlock extends BlockBase {

    /**
     * {@inheritdoc}
     */

    public function blockForm($form, FormStateInterface $form_state)
    {
        $form['config']['numero_documento'] = [
            '#title' => 'Ingrese numero de cedula',
            '#type' => 'textfield',
            '#default_value' => $this->configuration['config']['numero_documento'] ?? '1002860585',
        ];
        return $form;
    }
    /**
     * {@inheritdoc}
     */

    public function blockSubmit($form, FormStateInterface $form_state)
    {

    }

    /**
     * {@inheritdoc}
     */

    public function build(){

        $build = [
            '#theme' => 'conexionapi',
            '#attached' =>[
                'library'=>[
                    'conexion_api/conexion_api',
                ],
            ],
            '#plugin_id' => $this->getPluginId(),
            '#numero_documento' => '1002860585',
            '#codigo' => 'Este es u códgo',
            '#nombre' => 'Este es un nombre de MAeria',
            '#creditos' => 'Estos son los creditos',
            '#markup' => $this->getFrases(),
            '#cache' => [
              'max-age' => 0,
            ]
        ];
        

        return $build;
      }
    private function getFrases(){
        $frase = [
          'Hola, que tal',
          'Otras vez por aquí?',
          'Nos vemos pronto!!!'
        ];
        return $frase[array_rand($frase)];
       }

 }